package yaktracker

import javafx.application.Application.launch
import yaktracker.router.Router

fun main() {
    launch(Router::class.java)
}
