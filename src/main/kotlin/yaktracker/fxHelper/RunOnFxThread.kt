package yaktracker.fxHelper

import javafx.application.Platform

fun <T> runOnFxThread(body: Any.() -> T) {
    Platform.runLater {
        Any().body()
    }
}
