package yaktracker.userdata

import com.squareup.moshi.Moshi
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

class ConfigController {
    private val json = Moshi.Builder().build().adapter(ConfigSchema::class.java)

    fun storeYakServer(yakServer: String) {
        val configSchema = getConfigJson()
        configSchema.yakServer = yakServer
        File(getConfigPath()).writeText(json.toJson(configSchema))
    }

    fun getYakServer() = getConfigJson().yakServer

    private fun getConfigJson(): ConfigSchema {
        createDefaultJsonIfNotExists()
        return json.fromJson(File(getConfigPath()).readText()) ?: ConfigSchema("")
    }

    private fun createDefaultJsonIfNotExists() {
        if (!File(getConfigPath()).exists()) {
            Files.createFile(Paths.get(getConfigPath()))
            File(getConfigPath()).writeText(json.toJson(ConfigSchema("")))
        }
    }

    private fun getConfigPath() = Paths.get(TokenController.CONFIG_DIR, CONFIG_FILE).toString()
}
