package yaktracker.userdata

import io.github.soc.directories.ProjectDirectories
import java.io.File
import java.nio.file.Paths

class TokenController {
    companion object {
        val DATA_DIR: String = ProjectDirectories.from("dev", "yakstack", "yaktracker").dataDir
        val CONFIG_DIR: String = ProjectDirectories.from("dev", "yakstack", "yaktracker").configDir
    }

    init {
        File(DATA_DIR).mkdirs()
        File(CONFIG_DIR).mkdirs()
    }

    fun storeToken(token: String) = File(getTokenPath()).writeText(token)
    fun getToken() = File(getTokenPath()).readText()
    fun isLoggedIn() = File(getTokenPath()).exists()

    private fun getTokenPath() = Paths.get(CONFIG_DIR, TOKEN_FILE).toString()
}
