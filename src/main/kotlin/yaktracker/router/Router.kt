package yaktracker.router

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.layout.BorderPane
import javafx.scene.layout.VBox
import javafx.stage.Stage
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import yaktracker.auth.LOGIN_HEIGHT
import yaktracker.auth.LOGIN_WIDTH
import yaktracker.routingEvents.StartTasksEvent
import yaktracker.task.TASK_HEIGHT
import yaktracker.task.TASK_WIDTH
import yaktracker.userdata.ConfigController
import yaktracker.userdata.TokenController
import java.util.logging.Logger

class Router: Application() {

    private lateinit var primaryStage: Stage

    private val config = ConfigController()

    private val tokenController = TokenController()

    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    override fun start(primaryStage: Stage) {
        this.primaryStage = primaryStage
        EventBus.getDefault().register(this)
        // If the user isn't logged in, we should show them login
        if (!tokenController.isLoggedIn() || config.getYakServer() == "") {
            LOGGER.info("User is not authenticated, showing login window.")
            launchAuth()
        }

        // Just because the window exited doesn't mean they logged in
        // In the case they aren't we don't want to show them the next screens (they need a
        // valid token
        if (tokenController.isLoggedIn() || config.getYakServer() != "") {
            LOGGER.info("User is already authenticated. Showing task screen.")
            LOGGER.info("AuthStatus: ${tokenController.isLoggedIn()}, yakServer: ${config.getYakServer()}")
            launchTasks()
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onLaunchTasksEvent(event: StartTasksEvent) {
        LOGGER.info("Received startTaskEvent. Launching Task window.$event")
        launchTasks()
    }

    private fun launchTasks() {
        val fxmlLoader = FXMLLoader()
        fxmlLoader.location = ClassLoader.getSystemResource("Task.fxml")
        val root = fxmlLoader.load<BorderPane>()
        val scene = Scene(root, TASK_WIDTH, TASK_HEIGHT)
        scene.stylesheets.add(ClassLoader.getSystemResource("styles/bootstrap3.css").toExternalForm())
        primaryStage.scene = scene
        primaryStage.title = "YakTracker"
        primaryStage.show()
    }

    private fun launchAuth() {
        val fxmlLoader = FXMLLoader()
        fxmlLoader.location = ClassLoader.getSystemResource("Auth.fxml")
        val root = fxmlLoader.load<VBox>()
        val scene = Scene(root, LOGIN_WIDTH, LOGIN_HEIGHT)
        scene.stylesheets.add(ClassLoader.getSystemResource("styles/bootstrap3.css").toExternalForm())
        primaryStage.scene = scene
        primaryStage.title = "YakTracker - Login"
        primaryStage.show()
    }
}
