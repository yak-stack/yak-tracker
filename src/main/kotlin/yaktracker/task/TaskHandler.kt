package yaktracker.task

import dev.yakstack.transport.TaskServiceGrpc
import javafx.collections.FXCollections.observableArrayList
import javafx.scene.control.ListView
import java.util.logging.Logger

class TaskHandler(
    private val taskService: TaskServiceGrpc.TaskServiceStub,
    private val lv: ListView<String>
) {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }


    fun addTasks() {
        lv.items = observableArrayList(
            "hey",
            "there",
            "my",
            "dudes",
            "it's",
            "task",
            "time",
            "dingbat",
            "mode",
            "is",
            "at",
            "an",
            "all",
            "time",
            "high",
            "f",
            "e",
            "l",
            "l",
            "a",
            "s",
            "turning",
            "dingbat",
            "mode",
            "up",
            "to",
            "11",
            "turning",
            "dingbat",
            "mode",
            "up",
            "to",
            "12",
            "turning",
            "dingbat",
            "mode",
            "up",
            "to",
            "13",
            "turning",
            "dingbat",
            "mode",
            "up",
            "to",
            "14",
            "turning",
            "dingbat",
            "mode",
            "up",
            "to",
            "15"
        )
    }
}
