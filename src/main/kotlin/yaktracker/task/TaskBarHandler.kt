package yaktracker.task

import dev.yakstack.transport.TaskOuterClass
import dev.yakstack.transport.TaskServiceGrpc.TaskServiceStub
import io.grpc.stub.StreamObserver
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextField
import yaktracker.fxHelper.runOnFxThread
import yaktracker.userdata.TokenController
import java.util.logging.Logger

class TaskBarHandler {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    // Either we pass all these params in, we have everything in a huge god class,
    // or we make the consumer call each of the two functions below, which seems
    // error prone and silly because these two functions are ALWAYS called together
    @Suppress("LongParameterList")
    fun activateCurrentTrackingState(
        newTaskButton: Button,
        statusText: Label,
        categoryField: TextField,
        taskField: TextField,
        closeNewTaskField: Button,
        submitTaskButton: Button
    ) {
        toggleCurrentTrackingStateWidgets(
            true,
            newTaskButton,
            statusText
        )
        toggleNewTaskWidgets(
            false,
            categoryField,
            taskField,
            closeNewTaskField,
            submitTaskButton
        )
    }

    // Either we pass all these params in, we have everything in a huge god class,
    // or we make the consumer call each of the two functions below, which seems
    // error prone and silly because these two functions are ALWAYS called together
    @Suppress("LongParameterList")
    fun activateNewTaskState(
        newTaskButton: Button,
        statusText: Label,
        categoryField: TextField,
        taskField: TextField,
        closeNewTaskField: Button,
        submitTaskButton: Button
    ) {
        toggleCurrentTrackingStateWidgets(
            false,
            newTaskButton,
            statusText
        )
        toggleNewTaskWidgets(
            true,
            categoryField,
            taskField,
            closeNewTaskField,
            submitTaskButton
        )
    }

    private fun setSubmittingTask(submitTaskButton: Button) {
        runOnFxThread {
            submitTaskButton.text = "⌛"
            submitTaskButton.isDisable = true
        }
    }

    fun setSubmitTaskError(submitTaskButton: Button) {
        runOnFxThread {
            submitTaskButton.text = "❌ Error: Try Again"
            submitTaskButton.isDisable = false
        }
    }

    fun setSubmitTaskComplete(
        submitTaskButton: Button,
        categoryField: TextField,
        taskField: TextField
    ) {
        runOnFxThread {
            submitTaskButton.text =  "Submit Text"
            submitTaskButton.isDisable = false
            categoryField.text = ""
            taskField.text = ""
        }
    }

    // Either we pass all these params in, we have everything in a huge god class,
    // or we make the consumer call each of the two functions below, which seems
    // error prone and silly because these two functions are ALWAYS called together
    @Suppress("LongParameterList")
    fun submitTask(
        submitTaskButton: Button,
        categoryField: TextField,
        taskField: TextField,
        newTaskButton: Button,
        statusText: Label,
        closeNewTaskField: Button,
        userDataController: TokenController,
        taskService: TaskServiceStub
    ) {
        setSubmittingTask(submitTaskButton)
        taskService.submitTask(
            TaskOuterClass.TaskBundle.newBuilder()
                .addTasks(
                    TaskOuterClass.Task.newBuilder()
                        .setCategory(if (categoryField.text.isNullOrEmpty()) "Inbox" else categoryField.text)
                        .setTask(taskField.text)
                        .build()
                ).setToken(userDataController.getToken()).build(),
            object: StreamObserver<TaskOuterClass.TaskIdBundle> {
                override fun onNext(value: TaskOuterClass.TaskIdBundle?) {
                    LOGGER.info("Successfully submitted user task.")
                }

                override fun onError(t: Throwable?) {
                    setSubmitTaskError(submitTaskButton)
                    LOGGER.severe("Failed to push user task. ${t?.message}")
                }

                override fun onCompleted() {
                    LOGGER.info("Completed task submission")
                    setSubmitTaskComplete(
                        submitTaskButton,
                        categoryField,
                        taskField
                    )
                    activateCurrentTrackingState(
                        newTaskButton,
                        statusText,
                        categoryField,
                        taskField,
                        closeNewTaskField,
                        submitTaskButton
                    )
                }
            }
        )
    }

    private fun toggleCurrentTrackingStateWidgets(
        show: Boolean,
        newTaskButton: Button,
        statusText: Label
    ) {
        runOnFxThread {
            newTaskButton.isVisible = show
            newTaskButton.isManaged = show
            statusText.isVisible = show
            statusText.isManaged = show
        }
    }

    private fun toggleNewTaskWidgets(
        show: Boolean,
        categoryField: TextField,
        taskField: TextField,
        closeNewTaskField: Button,
        submitTaskButton: Button
    ) {
        runOnFxThread {
            categoryField.isVisible = show
            categoryField.isManaged = show
            categoryField.requestFocus()
            taskField.isVisible = show
            taskField.isManaged = show
            closeNewTaskField.isVisible = show
            closeNewTaskField.isManaged = show
            submitTaskButton.isVisible = show
            submitTaskButton.isManaged = show
        }
    }
}
