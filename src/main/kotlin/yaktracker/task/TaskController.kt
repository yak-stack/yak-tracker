package yaktracker.task

import dev.yakstack.transport.TaskServiceGrpc
import dev.yakstack.transport.TaskServiceGrpc.TaskServiceStub
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import org.controlsfx.validation.ValidationSupport
import org.controlsfx.validation.Validator
import yaktracker.transport.ChannelHelper
import yaktracker.userdata.ConfigController
import yaktracker.userdata.TokenController
import java.net.URL
import java.util.*
import java.util.logging.Logger

class TaskController: Initializable {
    private val userDataController = TokenController()

    private val taskService: TaskServiceStub by lazy { initializeTaskService() }

    private val taskBarHandler = TaskBarHandler()

    private lateinit var categoryHandler: CategoryHandler

    private lateinit var taskHandler: TaskHandler

    private val config = ConfigController()

    @FXML
    lateinit var statusText: Label

    @FXML
    lateinit var newTaskButton: Button

    @FXML
    lateinit var categoryField: TextField

    @FXML
    lateinit var taskField: TextField

    @FXML
    lateinit var closeNewTaskField: Button

    @FXML
    lateinit var submitTaskButton: Button

    @FXML
    lateinit var categoryList: ListView<String>

    @FXML
    lateinit var taskList: ListView<String>

    private val newTaskValidator = ValidationSupport()

    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        newTaskValidator.registerValidator(
            taskField,
            Validator.createEmptyValidator<TextField>("Task must be filled out!")
        )

        categoryHandler = CategoryHandler(taskService, categoryList)
        taskHandler = TaskHandler(taskService, taskList)
        categoryHandler.addCategories()
        taskHandler.addTasks()
    }

    fun newTaskClicked() {
        taskBarHandler.activateNewTaskState(
            newTaskButton,
            statusText,
            categoryField,
            taskField,
            closeNewTaskField,
            submitTaskButton
        )
    }

    fun closeNewTaskFieldsClicked() {
        taskBarHandler.activateCurrentTrackingState(
            newTaskButton,
            statusText,
            categoryField,
            taskField,
            closeNewTaskField,
            submitTaskButton
        )
    }

    fun submitTask() {
        // validate task and category
        // perhaps pop up window if new category
        // Smash that like button and push to backend

        if (!newTaskValidator.isInvalid) {
            taskBarHandler.submitTask(
                submitTaskButton,
                categoryField,
                taskField,
                newTaskButton,
                statusText,
                closeNewTaskField,
                userDataController,
                taskService
            )
        } else {
            newTaskValidator.redecorate()
        }
    }

    private fun initializeTaskService(): TaskServiceStub =
        TaskServiceGrpc.newStub(
            ChannelHelper().getChannel(config.getYakServer())
        )
}
