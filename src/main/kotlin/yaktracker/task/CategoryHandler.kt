package yaktracker.task

import dev.yakstack.transport.TaskServiceGrpc
import javafx.collections.FXCollections.observableArrayList
import javafx.scene.control.ListView
import java.util.logging.Logger

class CategoryHandler(
    private val taskService: TaskServiceGrpc.TaskServiceStub,
    private val lv: ListView<String>
) {
    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }


    fun addCategories() {
        lv.items = observableArrayList("hey", "there", "my", "dudes", "it's", "category", "time", "important todo")
    }
}
