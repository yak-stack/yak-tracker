package yaktracker.transport

import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import yaktracker.auth.SSL_PORT

class ChannelHelper {
    fun getChannel(url: String): ManagedChannel {
        val urlMetadata = getUrlMetadata(url)
        return if (urlMetadata.first == "localhost" || urlMetadata.first == "127.0.0.1") {
            ManagedChannelBuilder.forAddress(urlMetadata.first, urlMetadata.second).usePlaintext().build()
        } else {
            ManagedChannelBuilder.forAddress(urlMetadata.first, urlMetadata.second).build()
        }
    }

    fun getUrlMetadata(url: String): Pair<String, Int> {
        val splitUrl = url.split(":")
        return Pair(splitUrl[0], if (splitUrl.size > 1) {
            splitUrl[1].toInt()
        } else {
            SSL_PORT
        })
    }
}
