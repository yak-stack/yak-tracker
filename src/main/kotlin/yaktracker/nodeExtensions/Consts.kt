package yaktracker.nodeExtensions

const val DEFAULT_DELAY = 500.0
const val DEFAULT_SPEED = 0.7

const val FADE_OUT_SPEED = 6.5
const val FADE_IN_SPEED = 4.0
const val INSTANT_DELAY = 0.0
