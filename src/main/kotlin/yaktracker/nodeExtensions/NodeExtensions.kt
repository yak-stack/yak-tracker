package yaktracker.nodeExtensions

import animatefx.animation.FadeIn
import animatefx.animation.FadeOut
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.util.Duration

fun Node.toggleExistence() {
    val inverseVisible = !this.isVisible
    val inverseManaged = !this.isManaged
    val onFinishedHandler = EventHandler<ActionEvent> {
        this.isVisible = inverseVisible
        this.isManaged = inverseManaged
    }
    if (isVisible) {
        this.fadeOut(onFinishedHandler, INSTANT_DELAY, FADE_OUT_SPEED)
    } else {
        onFinishedHandler.handle(null)
        this.fadeIn(null, INSTANT_DELAY, FADE_IN_SPEED)
    }
}

fun Node.fadeIn(
    event: EventHandler<ActionEvent>? = null,
    delay: Double = DEFAULT_DELAY,
    speed: Double = DEFAULT_SPEED
) {
    FadeIn(this).setDelay(Duration(delay)).setSpeed(speed).also { anim ->
        event?.let { anim.setOnFinished(it) }
    }.play()
}

fun Node.fadeOut(
    event: EventHandler<ActionEvent>? = null,
    delay: Double = DEFAULT_DELAY,
    speed: Double = DEFAULT_SPEED
) {
    FadeOut(this).setDelay(Duration(delay)).setSpeed(speed).also { anim ->
        event?.let { anim.setOnFinished(it) }
    }.play()
}

fun fadeSet(vararg nodes: Node) {
    nodes.forEach { it.fadeIn() }
}

