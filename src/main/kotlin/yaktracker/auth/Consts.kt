package yaktracker.auth

const val LOGIN_WIDTH = 500.0
const val LOGIN_HEIGHT = 400.0
const val SSL_PORT = 443

const val FIELD_FILL_ERR = "Please fill out all fields and make sure server URL is correct"
