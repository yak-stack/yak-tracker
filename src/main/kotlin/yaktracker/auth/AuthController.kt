package yaktracker.auth

import dev.yakstack.transport.AuthServiceGrpc
import dev.yakstack.transport.Login
import dev.yakstack.transport.Register
import io.grpc.stub.StreamObserver
import javafx.application.Platform
import javafx.fxml.FXML
import javafx.fxml.Initializable
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.PasswordField
import javafx.scene.control.TextField
import javafx.scene.text.Text
import javafx.scene.text.TextFlow
import javafx.stage.Stage
import org.controlsfx.control.ToggleSwitch
import org.greenrobot.eventbus.EventBus
import yaktracker.nodeExtensions.fadeSet
import yaktracker.nodeExtensions.toggleExistence
import yaktracker.routingEvents.StartTasksEvent
import yaktracker.transport.ChannelHelper
import yaktracker.userdata.ConfigController
import yaktracker.userdata.TokenController
import java.lang.NumberFormatException
import java.net.URL
import java.util.*
import java.util.logging.Logger
import javax.mail.internet.AddressException
import javax.mail.internet.InternetAddress

// Most of these functions have to do with view manipulation
// While they could ostensibly move to different classes where the view is
// passed in, I think they're most appropriate here.
@Suppress("TooManyFunctions")
class AuthController: Initializable {
    @FXML
    lateinit var titleText: TextFlow

    @FXML
    private lateinit var emailLabel: Label

    @FXML
    private lateinit var emailField: TextField

    @FXML
    private lateinit var passwordLabel: Label

    @FXML
    private lateinit var passwordField: PasswordField

    @FXML
    private lateinit var firstNameField: TextField

    @FXML
    private lateinit var lastNameField: TextField

    @FXML
    private lateinit var firstNameLabel: Label

    @FXML
    private lateinit var lastNameLabel: Label

    @FXML
    private lateinit var authSwitch: ToggleSwitch

    @FXML
    lateinit var endpointLabel: Label

    @FXML
    lateinit var endpoint: TextField

    @FXML
    lateinit var errorText: Text

    @FXML
    lateinit var successText: Text

    @FXML
    lateinit var goButton: Button

    private val config = ConfigController()

    private val userDataController = TokenController()

    private val channelHelper = ChannelHelper()

    companion object {
        private val LOGGER = Logger.getLogger(this::class.java.name)
    }

    override fun initialize(location: URL?, resources: ResourceBundle?) {
        fadeSet(titleText,
            emailLabel, emailField,
            passwordLabel, passwordField,
            endpointLabel, endpoint,
            firstNameLabel, firstNameField,
            lastNameLabel, lastNameField,
            authSwitch,
            goButton
        )
        endpoint.text = config.getYakServer()
    }


    @FXML
    fun authClicked() {
        if (authSwitch.isSelected && checkLogin().also{ setErrorMessage(!it, FIELD_FILL_ERR) }) {
            val channel = channelHelper.getChannel(endpoint.text)
            enableGoButton(false)
            config.storeYakServer(endpoint.text)
            AuthServiceGrpc.newStub(channel)
                .login(
                    Login.LoginRequest.newBuilder()
                        .setEmail(emailField.text)
                        .setPassword(passwordField.text)
                        .setDeviceName("${System.getProperty("user.name")}'s " +
                            "YakTracker on ${System.getProperty("os.name")}")
                        .build(),
                    object: StreamObserver<Login.Token> {
                        override fun onNext(p0: Login.Token) {
                            LOGGER.info("Successfully authenticated")
                            userDataController.storeToken(p0.token)
                        }

                        override fun onError(p0: Throwable) {
                            LOGGER.info("Failed to authenticate for user: ${p0.message}")
                            channel.shutdown()
                            setErrorMessage(true, "${p0.message}")
                            enableGoButton(true)
                        }

                        override fun onCompleted() {
                            LOGGER.info("Completed login process for user")
                            channel.shutdown()
                            Platform.runLater {
                                (authSwitch.scene.window as Stage).close()
                                EventBus.getDefault().post(StartTasksEvent())
                            }
                        }
                    }
                )
        } else if (checkRegister().also { setErrorMessage(!it, FIELD_FILL_ERR) } ) {
            val channel = channelHelper.getChannel(endpoint.text)
            enableGoButton(false)
            AuthServiceGrpc.newStub(channel)
                .register(
                    Register.RegisterRequest.newBuilder()
                        .setEmail(emailField.text)
                        .setPassword(passwordField.text)
                        .setFirstName(firstNameField.text)
                        .setLastName(lastNameField.text)
                        .build(),
                    object: StreamObserver<Register.RegisterResponse> {
                        override fun onNext(p0: Register.RegisterResponse) {
                            LOGGER.info("Processed register request")
                        }

                        override fun onError(p0: Throwable) {
                            setErrorMessage(true, "${p0.message}")
                            channel.shutdown()
                            enableGoButton(true)
                        }

                        override fun onCompleted() {
                            channel.shutdown()
                            enableGoButton(true)
                            setSuccessMessage(true, "Successfully registered! Log in to start!")
                        }
                    }
                )
        }
    }

    private fun enableGoButton(state: Boolean) {
        goButton.isDisable = !state
    }

    @FXML
    fun switchClicked() {
        authSwitch.text = if (authSwitch.isSelected) {
            "Login"
        } else {
            "Register"
        }
        firstNameField.toggleExistence()
        firstNameLabel.toggleExistence()
        lastNameField.toggleExistence()
        lastNameLabel.toggleExistence()
        setErrorMessage(false, "")
        setSuccessMessage(false, "")
    }

    private fun checkRegister() = checkSetEmail() && checkSetPassword() && checkValidYakURL() && checkValidNames()

    private fun checkLogin() = checkSetEmail() && checkSetPassword() && checkValidYakURL()

    private fun setErrorMessage(show: Boolean, msg: String) {
        if (show) {
            setSuccessMessage(false, "")
        }
        errorText.text = msg
        errorText.isVisible = show
        errorText.isManaged = show
    }

    private fun setSuccessMessage(show: Boolean, msg: String) {
        if (show) {
            setErrorMessage(false, "")
        }
        successText.text = msg
        successText.isVisible = show
        successText.isManaged = show
    }

    private fun checkValidNames() = !firstNameField.text.isNullOrEmpty() && !lastNameField.text.isNullOrEmpty()

    private fun checkSetEmail(): Boolean {
        if (emailField.text.isNullOrEmpty()) {
            return false
        }

        return try {
            val emailAddr = InternetAddress(emailField.text)
            emailAddr.validate()
            true
        } catch (_: AddressException) {
            false
        }
    }

    private fun checkSetPassword() = !passwordField.text.isNullOrEmpty()

    private fun checkValidYakURL(): Boolean {
        return try {
            channelHelper.getUrlMetadata(endpoint.text)
            true
        } catch (_: NumberFormatException){
            false
        }
    }
}

